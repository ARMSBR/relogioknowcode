﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Relogio
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public INavigation Navigation { get; set; }

        private string horaDia = "12 00 00";
        public string HoraDia
        {
            get
            {
                return horaDia;
            }
            set
            {
                horaDia = value;
                NotifyPropertyChanged("HoraDia");
            }
        }

        public MainPageViewModel(INavigation navigation)
        {
            this.Navigation = navigation;
        }

        //Atualiza hora do relógio digital
        public void UpdateHour()
        {
            if (DateTime.Now.Second > 9)
            {
                HoraDia = DateTime.Now.Hour + " " + DateTime.Now.Minute + " " + DateTime.Now.Second;
            }
            else
            {
                HoraDia = DateTime.Now.Hour + " " + DateTime.Now.Minute + " 0" + DateTime.Now.Second;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
